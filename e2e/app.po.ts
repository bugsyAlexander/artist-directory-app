import { browser, element, by } from 'protractor';

export class ArtistDirectoryPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('ad-root h1')).getText();
  }
}
