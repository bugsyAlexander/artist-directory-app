import { ArtistDirectoryPage } from './app.po';

describe('artist-directory App', function() {
  let page: ArtistDirectoryPage;

  beforeEach(() => {
    page = new ArtistDirectoryPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('ad works!');
  });
});
