import { Component, Input } from '@angular/core';

@Component({
  selector: 'ad-artist-item',
  template: `
    <img src="images/{{artist.shortname}}_tn.jpg" alt="Photo of {{artist.name}}" class="artist-img">
    <div class="artist-info">
      <h2 class="artist-name">{{artist.name}}</h2>
      <h3 class="artist-reknown">{{artist.reknown}}</h3>
    </div>
  `,
  styleUrls: ['./artist-item.component.css']
})
export class ArtistItemComponent {
  @Input() artist;
}
