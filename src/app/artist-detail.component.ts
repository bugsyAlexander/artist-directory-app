import { Component, Input } from '@angular/core';

@Component({
  selector: 'ad-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.css']
})
export class ArtistDetailComponent {
  @Input() artist;
}
